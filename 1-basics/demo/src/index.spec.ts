const { divide } = require('./index');

describe('Test index.js', () => {
  describe('Test function divide', () => {
    it('Should return 2', () => {
      expect(divide(6, 3)).toEqual(2);
    });
    it('Throw an error when divide 0', () => {
      try {
        divide(6, 0);
      } catch (error) {
        expect(error).toThrow(Error);
      }
    });
  });
});
